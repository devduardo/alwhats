<div class="card card-profile shadow">
    <div class="px-4">
        <div class="mt-2">
            <h5><i class="lni lni-coin"></i> <?php echo e(__('Total')); ?><span class="font-weight-light"></span></h5>
      </div>
      <div  class="border-top">
        <!-- Price overview -->
        <div id="totalPrices" v-cloak>
            <div class="card card-stats mb-1 mb-xl-0 border-0">
                <div class="card-body bg-white pl-0">
                    <div class="row">
                        <div class="col">
                            <span v-if="totalPrice==0"><?php echo e(__('Cart is empty')); ?>!</span>

                           <h5><span v-if="totalPrice"><strong><?php echo e(__('Subtotal')); ?>:</strong></span>
                            <span v-if="totalPrice" class="ammount"><strong>{{ totalPriceFormat }}</strong></span></h5>
                            <?php if(config('app.isft')): ?>
                                <span v-if="totalPrice&&delivery"><br /><strong><?php echo e(__('Delivery')); ?>:</strong></span>
                                <span v-if="totalPrice&&delivery" class="ammount"><strong>{{ deliveryPriceFormated }}</strong></span><br />
                            <?php endif; ?>
                            <h4> <span v-if="totalPrice"><strong><?php echo e(__('Total')); ?>:</strong></span>
                            <span v-if="totalPrice" class="ammount"><strong>{{ withDeliveryFormat   }}</strong></span>
                            <input v-if="totalPrice" type="hidden" id="tootalPricewithDeliveryRaw" :value="withDelivery" /></h4>

                            <small class="text-muted">Delivery no incluido.</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Payment  Methods -->
        

        <!-- END Payment -->

        <!-- Payment Actions -->
        <?php if(!config('settings.social_mode')): ?>

            <!-- COD -->
            <?php echo $__env->make('cart.payments.cod', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <!-- Extra Payments ( Via module ) -->
            <?php $__currentLoopData = $extraPayments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $extraPayment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php echo $__env->make($extraPayment.'::button', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </form>

            <!-- Stripe -->
            <?php echo $__env->make('cart.payments.stripe', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>



        <?php elseif(config('settings.is_whatsapp_ordering_mode')): ?>
            <?php echo $__env->make('cart.payments.whatsapp', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php elseif(config('settings.is_facebook_ordering_mode')): ?>
            <?php echo $__env->make('cart.payments.facebook', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php endif; ?>
        <!-- END Payment Actions -->

        <div class="text-center" hidden>
            <div class="custom-control custom-checkbox mb-3" hidden>
                <input class="custom-control-input" id="privacypolicy" type="checkbox" checked>
                <!--<label class="custom-control-label" for="privacypolicy"><?php echo e(__('Acepto los Términos y condiciones y la Política de privacidad')); ?></label>-->
                
            </div>
        </div>

      </div>
      <br />
      <br />
    </div>
  </div>

  <?php if(config('settings.is_demo') && config('settings.enable_stripe')): ?>
    <?php echo $__env->make('cart.democards', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
  <?php endif; ?>
<?php /**PATH C:\laragon\www\alws\resources\views/cart/payment.blade.php ENDPATH**/ ?>