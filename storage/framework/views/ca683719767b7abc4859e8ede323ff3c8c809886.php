

<?php $__env->startSection('content'); ?>
 <div class="container">
    <div class="row justify-content-center align-items-center" style="height:80vh">
    <div class="col-sm-12 col-lg-3">
                <div class="card shadow">
                    <div class="card-body bg-white">
                        <div class="d-flex justify-content-center">
                            <img src="<?php echo e(config('global.site_logo')); ?>" width="220" class="thumbnail mb-3" alt="...">
                        </div>

                        <?php if(session('status')): ?>
                            <div class="alert alert-success" role="alert">
                                <?php echo e(session('status')); ?>

                            </div>
                        <?php endif; ?>
                        <p class="text-center mb-3">
                        <small> Escribe el correo electrónico que usaste para registrarte. Te enviaremos un correo electrónico con instrucciones sobre cómo restablecer tu contraseña.</small>
                        </p>
                        <form role="form" method="POST" action="<?php echo e(route('password.email')); ?>">
                            <?php echo csrf_field(); ?>

                            <div class="form-group<?php echo e($errors->has('email') ? ' has-danger' : ''); ?> mb-3">
                                <div class="input-group">
                                    <input class="form-control <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" placeholder="<?php echo e(__('Correo electrónico')); ?>" type="email" name="email" value="<?php echo e(old('email')); ?>" required autofocus>
                                </div>
                                <?php if($errors->has('email')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-block"><?php echo e(__('Send email')); ?></button>

                            </div>
                            <div class="text-center mt-2">
                                <small> <span class="text-muted">¿Recordaste tu contraseña?</span> <strong><a href="<?php echo e(route('login')); ?>"><?php echo e(__('Log in')); ?></a></strong> </small>
                               </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front', ['class' => 'bg-neu'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\alws\resources\views/auth/passwords/email.blade.php ENDPATH**/ ?>