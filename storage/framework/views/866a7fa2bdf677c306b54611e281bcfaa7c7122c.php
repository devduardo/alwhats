<div class="card card-profile shadow">
    <div class="px-4">
      <div class="mt-2">
        <h5><i class="lni lni-scooter"></i> <?php echo e(__('Delivery o lo buscas?')); ?><span class="font-weight-light"></span></h5>
      </div>
      <div class="card-content border-top">
        <div class="custom-control custom-radio mb-3 mt-2">
          <input name="deliveryType" class="custom-control-input" id="deliveryTypeDeliver" type="radio" value="delivery" checked>
          <label class="custom-control-label" for="deliveryTypeDeliver"><?php echo e(__('Delivery')); ?> <i class="text-muted">(costo no incluido)</i></label>
        </div>
        <div class="custom-control custom-radio mb-3">
          <input name="deliveryType" class="custom-control-input" id="deliveryTypePickup" type="radio" value="pickup">
          <label class="custom-control-label" for="deliveryTypePickup"><?php echo e(__('Yo lo Busco')); ?></label>
        </div>

      </div>

    </div>
  </div>
  <br>
<?php /**PATH C:\laragon\www\alws\resources\views/cart/delivery.blade.php ENDPATH**/ ?>