<div class="card card-profile shadow mb-4" id="clientInfo">
    <div class="px-4">
      <div class="mt-2">
        <h5><i class="lni lni-user"></i> <?php echo e(__('Tu Nombre')); ?><span class="font-weight-light"></span></h5>
      </div>
      <div class="card-content border-top">

        <?php echo $__env->make('partials.fields',['fields'=>[
            ['ftype'=>'input','name'=>"",'id'=>"client_name",'placeholder'=>"Nombre Completo",'required'=>false],
        ]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
      </div>
    </div>
</div>

<?php /**PATH C:\laragon\www\alws\resources\views/cart/client.blade.php ENDPATH**/ ?>