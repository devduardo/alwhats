<div class="header pb-7 pt-2 pt-lg-7 d-flex align-items-center" style="background-size: cover; background-position: center top;">
    <!-- Mask -->
    <span class=""></span>
    <!-- Header container -->
    <div class="container-fluid d-flex align-items-center">
        <div class="row">
            <div class="col-md-12 <?php echo e($class ?? ''); ?>">
                <h3 class=""><?php echo e($title); ?></h3>
                <?php if(isset($description) && $description): ?>
                    <p class="mt-0 mb-5"><?php echo e($description); ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php /**PATH C:\laragon\www\alws\resources\views/items/partials/header.blade.php ENDPATH**/ ?>