<ul class="navbar-nav mt-5">
    <?php if(config('app.ordering')): ?>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('home')); ?>">
                <i class="lni lni-display-alt"></i> <?php echo e(__('Dashboard')); ?>

            </a>
        </li>
        <?php if(config('app.isft')): ?>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('orders.index')); ?>">
                <i class="ni ni-basket text-orange"></i> <?php echo e(__('Orders')); ?>

            </a>
        </li>
        <?php endif; ?>
    <?php endif; ?>

        <?php if(config('app.isft')): ?>
        <li class="nav-item">
            <a class="nav-link" href="/live">
                <i class="ni ni-basket text-success"></i> <?php echo e(__('Live Orders')); ?><div class="blob red"></div>
            </a>
        </li>  
        
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('clients.index')); ?>">
                <i class="ni ni-single-02 text-blue"></i> <?php echo e(__('Clients')); ?>

            </a>
        </li><?php endif; ?>
      <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('drivers.index')); ?>">
                <i class="ni ni-delivery-fast"></i> <?php echo e(__('Drivers')); ?>

            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('admin.restaurants.index')); ?>">
                <i class="lni lni-restaurant"></i> <?php echo e(__('Restaurants')); ?>

            </a>
        </li>
        <?php if(config('app.isft')): ?>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('reviews.index')); ?>">
                <i class="ni ni-diamond text-info"></i> <?php echo e(__('Reviews')); ?>

            </a>
        </li>
        <?php endif; ?>
        <?php if(config('settings.multi_city')): ?>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('cities.index')); ?>">
                <i class="ni ni-building text-orange"></i> <?php echo e(__('Cities')); ?>

            </a>
        </li>
        <?php endif; ?>
        
        <?php if(config('settings.enable_pricing')): ?>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('plans.index')); ?>">
                <i class="lni lni-money-protection"></i> <?php echo e(__('Pricing plans')); ?>

            </a>
        </li>
        <?php endif; ?>
        <?php if(config('app.ordering')&&config('settings.enable_finances_admin')): ?>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('finances.admin')); ?>">
                <i class="ni ni-money-coins"></i> <?php echo e(__('Finances')); ?>

            </a>
        </li>
        <?php endif; ?>

        <?php if(config('settings.app_dev')): ?>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('admin.restaurant.banners.index')); ?>">
                <i class="ni ni-album-2 text-green"></i> <?php echo e(__('Banners')); ?>

            </a>
         </li>
         <?php endif; ?>
        <?php if(config('app.isqrsaas')): ?>
            <?php if(config('settings.is_whatsapp_ordering_mode')): ?>
            
            <?php endif; ?>
        <li class="nav-item">
            <?php
                $theLocaleToOpen=strtolower(config('settings.app_locale'));
                if( strtolower(session('applocale_change')).""!=""){
                    $theLocaleToOpen=strtolower(session('applocale_change'));
                }
            ?>
            
        </li>
        <?php else: ?>
        
        <?php endif; ?>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('settings.index')); ?>">
                <i class="lni lni-cog"></i> <?php echo e(__('Site Settings ')); ?>

            </a>
        </li>

        <?php if(!config('settings.hideApps')): ?>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(route('apps.index')); ?>">
                    <i class="ni ni-spaceship"></i> <?php echo e(__('Apps')); ?>

                </a>
            </li>
        <?php endif; ?>


        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('settings.cloudupdate')); ?>">
                <i class="lni lni-cloud-download"></i></i> <?php echo e(__('Updates')); ?>

            </a>
        </li>
</ul>
<div class="pt-5">
<small class="text-muted text-light">
    With <i class="lni lni-heart text-danger"></i> by. Code and Hardware.</a>
</small>
</div>
<?php /**PATH C:\laragon\www\alws\resources\views/layouts/navbars/menus/admin.blade.php ENDPATH**/ ?>