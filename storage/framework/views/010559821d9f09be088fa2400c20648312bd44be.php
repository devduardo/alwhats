
<?php if(strlen(config('settings.recaptcha_site_key'))>2): ?>
    <?php $__env->startSection('head'); ?>
    <?php echo htmlScriptTagJsApi([]); ?>

    <?php $__env->stopSection(); ?>
<?php endif; ?>
<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center align-items-center" style="height:80vh">
        <div class="col-sm-12 col-lg-3">
                <div class="card shadow">
                    <div class="card-body bg-white">
                        <div class="d-flex justify-content-center">
                            <img src="<?php echo e(config('global.site_logo')); ?>" width="150" class=" mb-3" alt="...">
                        </div>
                        <form  id="registerform" method="post" action="<?php echo e(route('newrestaurant.store')); ?>" autocomplete="off">
                            <?php echo csrf_field(); ?>
                            <?php if(session('status')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo e(session('status')); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php endif; ?>
                            <div class="">
                                <div class="form-group mb-3<?php echo e($errors->has('name') ? ' has-danger' : ''); ?>">
                                <div class="input-group">
                                    <input type="text" name="name" id="name" class="form-control <?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" placeholder="<?php echo e(__('Restaurant Name')); ?>" value="<?php echo e(isset($_GET["name"])?$_GET['name']:""); ?>" required autofocus>
                                </div>


                                </div>
                                <div class="form-group mb-3<?php echo e($errors->has('name_owner') ? ' has-danger' : ''); ?>">
                                    <div class="input-group">
                                    <input type="text" name="name_owner" id="name_owner" class="form-control <?php echo e($errors->has('name_owner') ? ' is-invalid' : ''); ?>" placeholder="<?php echo e(__('Owner Name')); ?>" value="<?php echo e(isset($_GET["name"])?$_GET['name']:""); ?>" required autofocus>
                                    </div>

                                </div>
                                <div class="form-group mb-3<?php echo e($errors->has('email_owner') ? ' has-danger' : ''); ?>">
                                    <input type="email" name="email_owner" id="email_owner" class="form-control <?php echo e($errors->has('email_owner') ? ' is-invalid' : ''); ?>" placeholder="<?php echo e(__('Owner Email')); ?>" value="<?php echo e(isset($_GET["email"])?$_GET['email']:""); ?>" required autofocus>

                                </div>
                                <div class="form-group mb-3<?php echo e($errors->has('phone_owner') ? ' has-danger' : ''); ?>">
                                    <input type="text" name="phone_owner" id="phone_owner" class="form-control <?php echo e($errors->has('phone_owner') ? ' is-invalid' : ''); ?>" placeholder="<?php echo e(__('Owner Phone')); ?>" value="<?php echo e(isset($_GET["phone"])?$_GET['phone']:""); ?>" required autofocus>
                                </div>
                                <div class="form-group mb-0">
                                    <input type="checkbox" class="" name="check" id="check" required autofocus>

                                    <label class="<?php echo e($errors->has('check') ? ' text-danger' : ''); ?>" for="check" > <small>He leído y acepto los <strong><a href="<?php echo e(route('login')); ?>" class="<?php echo e($errors->has('check') ? ' text-danger' : ''); ?>">términos del servicio</a></strong>.</small></label>

                                  </div>


                                <div class="text-center pt-0">
                                    <?php if(strlen(config('settings.recaptcha_site_key'))>2): ?>
                                        <?php if($errors->has('g-recaptcha-response')): ?>
                                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($errors->first('g-recaptcha-response')); ?></strong>
                                        </span>
                                        <?php endif; ?>

                                        <?php echo htmlFormButton(__('Register your restaurant'), ['id'=>'thesubmitbtn','class' => 'btn btn-primary btn-block']); ?>

                                    <?php else: ?>
                                        <button type="submit" id="thesubmitbtn" class="btn btn-primary btn-block" onclick="this.disabled=true;"><?php echo e(__('Register your restaurant')); ?></button>
                                    <?php endif; ?>



                            </div>
                            <div class="text-center mt-2">
                                <small> <span class="text-muted">¿Ya estas registrado?</span> <strong><a href="<?php echo e(route('login')); ?>"><?php echo e(__('Log in')); ?></a></strong> </small>
                               </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<?php if(isset($_GET['name'])&&$errors->isEmpty()): ?>
<script>
    "use strict";
    document.getElementById("thesubmitbtn").click();
</script>
<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front', ['title' => __('User Profile')], ['class' => 'bg-neu'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\alws\resources\views/restorants/register.blade.php ENDPATH**/ ?>