<div id="addressBox">
  <br/>
<div class="card card-profile shadow" >
    <div class="px-4">
      <div class="mt-2">
        <h5><i class="lni lni-pin"></i> <?php echo e(__('Ubicacion')); ?><span class="font-weight-light"></span></h5>
      </div>
      <div class="card-content border-top">
        <div>
          <?php echo $__env->make('partials.fields',['fields'=>[['ftype'=>'input','name'=>"",'id'=>"addressID",'placeholder'=>"Barriada o Urbanizacion", 'required'=>true]]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>

        <small class="text-muted">
      <p class="text-danger" style="font-size: 11px;"><i class="lni lni-invention"></i> Deberás compartir tu ubicación para una entrega más precisa.</p>
       </small>
      </div>
    </div>
</div>
</div>
<?php /**PATH C:\laragon\www\alws\resources\views/cart/newaddress.blade.php ENDPATH**/ ?>