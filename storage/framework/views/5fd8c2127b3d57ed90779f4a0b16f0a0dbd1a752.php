<div class="card card-profile shadow">
    <div class="px-4">
      <div class="mt-2">
        <h5><i class="lni lni-restaurant"></i> <?php echo e(__('Inf. del comercio')); ?><span class="font-weight-light"></span></h5>
      </div>
      <div class="card-content border-top">
        <div class="pl-lg-4 mb-3">
            <small>
              <img src="<?php echo e($restorant->logom); ?>" class="img-responsive lazy float-right">
              <b> Nombre:</b> <?php echo e($restorant->name); ?><br />
              <b>Direccion:</b>  <?php echo e($restorant->address); ?><br />
              <b> Whatsapp:</b> <?php echo e($restorant->phone); ?><br />
              <b> <?php echo e(__('Horarios')); ?>:</b> <?php echo e($openingTime . " - " . $closingTime); ?>

            </small>

      </div>
      </div>
    </div>
  </div>
  <br />
<?php /**PATH C:\laragon\www\alws\resources\views/cart/restaurant.blade.php ENDPATH**/ ?>