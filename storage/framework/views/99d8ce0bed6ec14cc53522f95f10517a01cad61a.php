<ul class="navbar-nav mt-5">
    <?php if(config('app.ordering')): ?>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('home')); ?>">
                <i class="lni lni-display-alt"></i> <?php echo e(__('Dashboard')); ?>

            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="/live">
            <i class="lni lni-alarm text-red"></i> <?php echo e(__('Live Orders')); ?><div class="blob red"></div>
            </a>
        </li>


        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('orders.index')); ?>">
            <i class="lni lni-alarm"></i> <?php echo e(__('Orders')); ?>

            </a>
        </li>
    <?php endif; ?>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('admin.restaurants.edit',  auth()->user()->restorant->id)); ?>">
        <i class="lni lni-restaurant"></i><?php echo e(__('Restaurant')); ?>

        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(route('items.index')); ?>">
            <i class="lni lni-popup"></i> <?php echo e(__('Menu')); ?>

        </a>
    </li>

    <?php if(auth()->check() && auth()->user()->hasRole('store')): ?>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(route('admin.restaurant.tables.index')); ?>">
                    <i class="ni ni-ungroup text-red"></i> <?php echo e(__('Tables')); ?>

                </a>
    <?php endif; ?>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('qr')); ?>">
                <i class="fas fa-qrcode"></i> <?php echo e(__('QR Builder')); ?>

            </a>
        </li>
    <?php if(config('app.isqrsaas')&&!config('settings.is_whatsapp_ordering_mode')): ?>
        
        <?php if(config('settings.enable_guest_log')): ?>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('admin.restaurant.visits.index')); ?>">
                <i class="ni ni-calendar-grid-58 text-blue"></i> <?php echo e(__('Customers log')); ?>

            </a>
        </li>
        <?php endif; ?>
    <?php endif; ?>

    <?php if(config('settings.enable_pricing')): ?>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('plans.current')); ?>">
            <i class="lni lni-credit-cards"></i> <?php echo e(__('Plan')); ?>

            </a>
        </li>
    <?php endif; ?>

        <?php if(config('app.ordering')&&config('settings.enable_finances_owner')): ?>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(route('finances.owner')); ?>">
                <i class="ni ni-money-coins"></i> <?php echo e(__('Finances')); ?>

                </a>
            </li>
        <?php endif; ?>
<li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('admin.restaurant.coupons.index')); ?>">
                <i class="ni ni-tag"></i> <?php echo e(__('Coupons')); ?>

            </a>
        </li> 
        
        

    <li class="nav-item">
            <a class="nav-link" href="<?php echo e(route('share.menu')); ?>">
                <i class="ni ni-send"></i> <?php echo e(__('Share')); ?>

            </a>
    </li> 

</ul>
<div class="pt-5">
    <small class="text-muted text-light">
        With <i class="lni lni-heart text-danger"></i> by. Code and Hardware.</a>
    </small>
    </div>
<?php /**PATH C:\laragon\www\alws\resources\views/layouts/navbars/menus/owner.blade.php ENDPATH**/ ?>