

<?php $__env->startSection('content'); ?>

<div class="container">
    <div class="row justify-content-center align-items-center" style="height:80vh">
        <div class="col-sm-12 col-lg-3">

                <?php if(session('status')): ?>
                    <div class="card bg-secondary shadow border-0">
                        <div class="card-body bg-white">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <?php echo e(session('status')); ?>

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>


                <div class="card shadow">
                    <div class="card-body bg-white">
                    <div class="d-flex justify-content-center">
                        <img src="<?php echo e(config('global.site_logo')); ?>" width="220" class=" mb-3" alt="...">
                    </div>
                    <?php if(session('status')): ?>
                    <div class="row">
                      <div class="col-8">
                        <div class="d-none d-lg-block mb-4">
                          <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <?php echo e(session('status')); ?>

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php endif; ?>
                        <form role="form" method="POST" action="<?php echo e(route('login')); ?>">
                            <?php echo csrf_field(); ?>
                            <div class="form-group<?php echo e($errors->has('email') ? ' has-danger' : ''); ?> mb-3">
                                <div class="input-group">
                                    <input class="form-control <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" placeholder="<?php echo e(__('Correo electrónico')); ?>" type="email" name="email" value="<?php echo e(old('email')); ?>" required autofocus>
                                </div>
                                <?php if($errors->has('email')): ?>
                                    <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="form-group<?php echo e($errors->has('password') ? ' has-danger' : ''); ?>">
                                <div class="input-group">
                                    <input class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" placeholder="<?php echo e(__('Contraseña')); ?>" type="password" required>
                                </div>
                                <?php if($errors->has('password')): ?>
                                    <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-block"><?php echo e(__('Log in')); ?></button>
                            </div>
                            <?php if(Route::has('password.request')): ?>
                            <div class="text-center mt-2">
                                <small><a href="<?php echo e(route('password.request')); ?>"><?php echo e(__('¿Olvidaste tu contraseña?')); ?></a></small>
                              </div>
                            <?php endif; ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front', ['class' => 'bg-neu'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\alws\resources\views/auth/login.blade.php ENDPATH**/ ?>