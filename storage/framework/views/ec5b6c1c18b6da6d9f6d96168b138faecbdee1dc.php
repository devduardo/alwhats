<br />
<div class="card card-profile shadow">
    <div class="px-4">
      <div class="mt-2">
        <h5><i class="lni lni-comments"></i> <?php echo e(__('Algún comentario')); ?><span class="font-weight-light"></span></h5>
      </div>
      <div class="card-content border-top">

        <div class="form-group<?php echo e($errors->has('comment') ? ' has-danger' : ''); ?>">
            <textarea name="comment" id="comment" class="form-control<?php echo e($errors->has('comment') ? ' is-invalid' : ''); ?>" placeholder="<?php echo e(__( 'Comentarios acerca de tu pedido' )); ?> ..."></textarea>
            <?php if($errors->has('comment')): ?>
                <span class="invalid-feedback" role="alert">
                    <strong><?php echo e($errors->first('comment')); ?></strong>
                </span>
            <?php endif; ?>
        </div>
      </div>
    </div>
</div>
<br />
<?php /**PATH C:\laragon\www\alws\resources\views/cart/comment.blade.php ENDPATH**/ ?>