<div class="card card-profile shadow mb-4">
    <div class="px-4">
      <div class="mt-2">
        <h5><i class="lni lni-timer"></i> <span class="delTime delTimeTS"><?php echo e(__('Delivery time')); ?></span><span class="picTime picTimeTS"><?php echo e(__('Pickup time')); ?></span><span class="font-weight-light"></span></h5>
      </div>
      <div class="card-content border-top">
        <br />
        <select name="timeslot" id="timeslot" class="form-control<?php echo e($errors->has('timeslot') ? ' is-invalid' : ''); ?>">
          <?php $__currentLoopData = $timeSlots; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value => $text): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value=<?php echo e($value); ?>><?php echo e($text); ?></option>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </select>
      </div>
      <br />
    </div>
  </div>

<?php /**PATH C:\laragon\www\alws\resources\views/cart/time.blade.php ENDPATH**/ ?>