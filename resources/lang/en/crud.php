<?php

return [
    'enter_item_name' => 'Ingresar nombre :item',
    'item_managment'=>'Gestión de :Item',
    'add_new_item'=>'Añadir nuevo :item',
    'new_item'=>'Nuevo :item',
    'back'=>'Regresar',
    'item_has_been_added'=>':Se ha añadido el artículo.',
    'edit_item_name'=>'Editar :item :name',
    'item_has_been_updated'=>':Item Ha sido actualizado',
    'item_has_been_removed'=>':Item ha sido removido',
    'item_has_items_associated'=>':Item tiene elementos asociados. Elimina los elementos asociados antes de eliminar este. :item.',
    'edit'=>'Editar',
    'delete'=>'Borrar',
    'actions'=>'Acciones',
    'no_items'=>'No hay :items...',
    'clear_filters'=>'Limpiar filtros',
    'download_report'=>'Descargar informe',
    'filter'=>'Filtro',
];
