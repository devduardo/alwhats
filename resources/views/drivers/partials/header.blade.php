<div class="header pb-7 pt-2 pt-lg-7 d-flex align-items-center" style="background-size: cover; background-position: center top;">
    <!-- Mask -->
    <span class=""></span>
    <!-- Header container -->
    <div class="container-fluid d-flex align-items-center">
        <div class="row">
            <div class="col-md-12 {{ $class ?? '' }}">
                <h3 class="">{{ $title }}</h3>
                @if (isset($description) && $description)
                    <p class="mt-0 mb-5">{{ $description }}</p>
                @endif
            </div>
        </div>
    </div>
</div>
