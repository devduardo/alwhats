@extends('layouts.front', ['class' => 'bg-neu'])

@section('content')

<div class="container">
    <div class="row justify-content-center align-items-center" style="height:80vh">
        <div class="col-sm-12 col-lg-3">

                @if (session('status'))
                    <div class="card bg-secondary shadow border-0">
                        <div class="card-body bg-white">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                @endif


                <div class="card shadow">
                    <div class="card-body bg-white">
                    <div class="d-flex justify-content-center">
                        <img src="{{ config('global.site_logo') }}" width="220" class=" mb-3" alt="...">
                    </div>
                    @if (session('status'))
                    <div class="row">
                      <div class="col-8">
                        <div class="d-none d-lg-block mb-4">
                          <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  @endif
                        <form role="form" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }} mb-3">
                                <div class="input-group">
                                    <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Correo electrónico') }}" type="email" name="email" value="{{ old('email') }}" required autofocus>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                <div class="input-group">
                                    <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ __('Contraseña') }}" type="password" required>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-block">{{ __('Log in') }}</button>
                            </div>
                            @if (Route::has('password.request'))
                            <div class="text-center mt-2">
                                <small><a href="{{ route('password.request') }}">{{ __('¿Olvidaste tu contraseña?') }}</a></small>
                              </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection
