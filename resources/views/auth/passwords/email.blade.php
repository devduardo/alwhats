@extends('layouts.front', ['class' => 'bg-neu'])

@section('content')
 <div class="container">
    <div class="row justify-content-center align-items-center" style="height:80vh">
    <div class="col-sm-12 col-lg-3">
                <div class="card shadow">
                    <div class="card-body bg-white">
                        <div class="d-flex justify-content-center">
                            <img src="{{ config('global.site_logo') }}" width="220" class="thumbnail mb-3" alt="...">
                        </div>

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <p class="text-center mb-3">
                        <small> Escribe el correo electrónico que usaste para registrarte. Te enviaremos un correo electrónico con instrucciones sobre cómo restablecer tu contraseña.</small>
                        </p>
                        <form role="form" method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }} mb-3">
                                <div class="input-group">
                                    <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Correo electrónico') }}" type="email" name="email" value="{{ old('email') }}" required autofocus>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary btn-block">{{ __('Send email') }}</button>

                            </div>
                            <div class="text-center mt-2">
                                <small> <span class="text-muted">¿Recordaste tu contraseña?</span> <strong><a href="{{ route('login') }}">{{ __('Log in') }}</a></strong> </small>
                               </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
    </div>
@endsection
