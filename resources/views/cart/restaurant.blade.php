<div class="card card-profile shadow">
    <div class="px-4">
      <div class="mt-2">
        <h5><i class="lni lni-restaurant"></i> {{ __('Inf. del comercio') }}<span class="font-weight-light"></span></h5>
      </div>
      <div class="card-content border-top">
        <div class="pl-lg-4 mb-3">
            <small>
              <img src="{{ $restorant->logom }}" class="img-responsive lazy float-right">
              <b> Nombre:</b> {{ $restorant->name }}<br />
              <b>Direccion:</b>  {{ $restorant->address }}<br />
              <b> Whatsapp:</b> {{ $restorant->phone }}<br />
              <b> {{ __('Horarios') }}:</b> {{ $openingTime . " - " . $closingTime }}
            </small>

      </div>
      </div>
    </div>
  </div>
  <br />
