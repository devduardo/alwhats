@if(count($fieldsToRender)>0)
<div class="card card-profile shadow">
    <div class="px-4">
      <div class="mt-2">
        <h5><i class="lni lni-user"></i> {{ __(config('settings.label_on_custom_fields')) }}<span class="font-weight-light"></span></h5>
      </div>
      <div class="card-content border-top">
        @include('partials.fields',['fields'=>$fieldsToRender])
      </div>
    </div>
</div>

@endif
