<div class="text-center" id="totalSubmitCOD">
    <button v-if="totalPrice" type="submit" class="btn btn-lg btn-block btn-icon btn-success paymentbutton bg-alws"
        onclick="this.disabled=true;this.form.submit();">
    <span class="btn-inner--icon lg"><i class="lni lni-whatsapp"></i></span>
    <span class="btn-inner--text">{{ __('Enviar alwhats!') }}</span>
    </button>
    <small class="text-center"><i class="lni lni-chevron-left-circle"></i> <a href="javascript:history.go(-1)">Olvidaste algo? Regresa al Catalogo.</a></small>
</div>


