<div class="card card-profile shadow mb-4" id="clientInfo">
    <div class="px-4">
      <div class="mt-2">
        <h5><i class="lni lni-user"></i> {{ __('Tu Nombre') }}<span class="font-weight-light"></span></h5>
      </div>
      <div class="card-content border-top">

        @include('partials.fields',['fields'=>[
            ['ftype'=>'input','name'=>"",'id'=>"client_name",'placeholder'=>"Nombre Completo",'required'=>false],
        ]])
      </div>
    </div>
</div>

