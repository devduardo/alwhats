<div id="addressBox">
  <br/>
<div class="card card-profile shadow" >
    <div class="px-4">
      <div class="mt-2">
        <h5><i class="lni lni-pin"></i> {{ __('Ubicacion') }}<span class="font-weight-light"></span></h5>
      </div>
      <div class="card-content border-top">
        <div>
          @include('partials.fields',['fields'=>[['ftype'=>'input','name'=>"",'id'=>"addressID",'placeholder'=>"Barriada o Urbanizacion", 'required'=>true]]])
        </div>

        <small class="text-muted">
      <p class="text-danger" style="font-size: 11px;"><i class="lni lni-invention"></i> Deberás compartir tu ubicación para una entrega más precisa.</p>
       </small>
      </div>
    </div>
</div>
</div>
