@extends('layouts.front', ['title' => __('User Profile')], ['class' => 'bg-neu'])
@if (strlen(config('settings.recaptcha_site_key'))>2)
    @section('head')
    {!! htmlScriptTagJsApi([]) !!}
    @endsection
@endif
@section('content')
<div class="container">
    <div class="row justify-content-center align-items-center" style="height:80vh">
        <div class="col-sm-12 col-lg-3">
                <div class="card shadow">
                    <div class="card-body bg-white">
                        <div class="d-flex justify-content-center">
                            <img src="{{ config('global.site_logo') }}" width="150" class=" mb-3" alt="...">
                        </div>
                        <form  id="registerform" method="post" action="{{ route('newrestaurant.store') }}" autocomplete="off">
                            @csrf
                            @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <div class="">
                                <div class="form-group mb-3{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <div class="input-group">
                                    <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Restaurant Name') }}" value="{{ isset($_GET["name"])?$_GET['name']:""}}" required autofocus>
                                </div>


                                </div>
                                <div class="form-group mb-3{{ $errors->has('name_owner') ? ' has-danger' : '' }}">
                                    <div class="input-group">
                                    <input type="text" name="name_owner" id="name_owner" class="form-control {{ $errors->has('name_owner') ? ' is-invalid' : '' }}" placeholder="{{ __('Owner Name') }}" value="{{ isset($_GET["name"])?$_GET['name']:""}}" required autofocus>
                                    </div>

                                </div>
                                <div class="form-group mb-3{{ $errors->has('email_owner') ? ' has-danger' : '' }}">
                                    <input type="email" name="email_owner" id="email_owner" class="form-control {{ $errors->has('email_owner') ? ' is-invalid' : '' }}" placeholder="{{ __('Owner Email') }}" value="{{ isset($_GET["email"])?$_GET['email']:""}}" required autofocus>

                                </div>
                                <div class="form-group mb-3{{ $errors->has('phone_owner') ? ' has-danger' : '' }}">
                                    <input type="text" name="phone_owner" id="phone_owner" class="form-control {{ $errors->has('phone_owner') ? ' is-invalid' : '' }}" placeholder="{{ __('Owner Phone') }}" value="{{ isset($_GET["phone"])?$_GET['phone']:""}}" required autofocus>
                                </div>
                                <div class="form-group mb-0">
                                    <input type="checkbox" class="" name="check" id="check" required autofocus>

                                    <label class="{{ $errors->has('check') ? ' text-danger' : '' }}" for="check" > <small>He leído y acepto los <strong><a href="{{ route('login') }}" class="{{ $errors->has('check') ? ' text-danger' : '' }}">términos del servicio</a></strong>.</small></label>

                                  </div>


                                <div class="text-center pt-0">
                                    @if (strlen(config('settings.recaptcha_site_key'))>2)
                                        @if ($errors->has('g-recaptcha-response'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                        @endif

                                        {!! htmlFormButton(__('Register your restaurant'), ['id'=>'thesubmitbtn','class' => 'btn btn-primary btn-block']) !!}
                                    @else
                                        <button type="submit" id="thesubmitbtn" class="btn btn-primary btn-block" onclick="this.disabled=true;">{{__('Register your restaurant')}}</button>
                                    @endif



                            </div>
                            <div class="text-center mt-2">
                                <small> <span class="text-muted">¿Ya estas registrado?</span> <strong><a href="{{ route('login') }}">{{ __('Log in') }}</a></strong> </small>
                               </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('js')
@if (isset($_GET['name'])&&$errors->isEmpty())
<script>
    "use strict";
    document.getElementById("thesubmitbtn").click();
</script>
@endif
@endsection
