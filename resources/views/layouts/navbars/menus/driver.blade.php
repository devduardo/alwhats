<ul class="navbar-nav mt-5">
    <li class="nav-item">
            <a class="nav-link" href="{{ route('home') }}">
                <i class="ni ni-tv-2"></i> {{ __('Dashboard') }}
            </a>
        </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('orders.index') }}">
            <i class="ni ni-basket"></i> {{ __('Orders') }}
        </a>
    </li>
</ul>
