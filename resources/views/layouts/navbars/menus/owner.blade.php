<ul class="navbar-nav mt-5">
    @if(config('app.ordering'))
        <li class="nav-item">
            <a class="nav-link" href="{{ route('home') }}">
                <i class="lni lni-display-alt"></i> {{ __('Dashboard') }}
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="/live">
            <i class="lni lni-alarm text-red"></i> {{ __('Live Orders') }}<div class="blob red"></div>
            </a>
        </li>


        <li class="nav-item">
            <a class="nav-link" href="{{ route('orders.index') }}">
            <i class="lni lni-alarm"></i> {{ __('Orders') }}
            </a>
        </li>
    @endif

    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.restaurants.edit',  auth()->user()->restorant->id) }}">
        <i class="lni lni-restaurant"></i>{{ __('Restaurant') }}
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('items.index') }}">
            <i class="lni lni-popup"></i> {{ __('Menu') }}
        </a>
    </li>

    @hasrole('store')
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.restaurant.tables.index') }}">
                    <i class="ni ni-ungroup text-red"></i> {{ __('Tables') }}
                </a>
    @endif
        <li class="nav-item">
            <a class="nav-link" href="{{ route('qr') }}">
                <i class="fas fa-qrcode"></i> {{ __('QR Builder') }}
            </a>
        </li>
    @if (config('app.isqrsaas')&&!config('settings.is_whatsapp_ordering_mode'))
        
        @if(config('settings.enable_guest_log'))
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.restaurant.visits.index') }}">
                <i class="ni ni-calendar-grid-58 text-blue"></i> {{ __('Customers log') }}
            </a>
        </li>
        @endif
    @endif

    @if(config('settings.enable_pricing'))
        <li class="nav-item">
            <a class="nav-link" href="{{ route('plans.current') }}">
            <i class="lni lni-credit-cards"></i> {{ __('Plan') }}
            </a>
        </li>
    @endif

        @if(config('app.ordering')&&config('settings.enable_finances_owner'))
            <li class="nav-item">
                <a class="nav-link" href="{{ route('finances.owner') }}">
                <i class="ni ni-money-coins"></i> {{ __('Finances') }}
                </a>
            </li>
        @endif
<li class="nav-item">
            <a class="nav-link" href="{{ route('admin.restaurant.coupons.index') }}">
                <i class="ni ni-tag"></i> {{ __('Coupons') }}
            </a>
        </li> 
        
        

    <li class="nav-item">
            <a class="nav-link" href="{{ route('share.menu') }}">
                <i class="ni ni-send"></i> {{ __('Share') }}
            </a>
    </li> 

</ul>
<div class="pt-5">
    <small class="text-muted text-light">
        With <i class="lni lni-heart text-danger"></i> by. Code and Hardware.</a>
    </small>
    </div>
